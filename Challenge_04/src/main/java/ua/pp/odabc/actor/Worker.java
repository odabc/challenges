package ua.pp.odabc.actor;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;

public class Worker extends UntypedActor {
    private final ActorRef listener;

    public Worker(ActorRef listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof String) {
            String[] pair = ((String) message).split(";");
            try {
                int key = Integer.parseInt(pair[0]);
                double value = Double.parseDouble(pair[1]);
                listener.tell(Message.get(key, value), getSelf());
            } catch (NumberFormatException | IndexOutOfBoundsException exception) {
                unhandled(message);
            }
        } else {
            unhandled(message);
        }
    }
}
