package ua.pp.odabc.actor;

import java.io.Serializable;

public enum Message implements Serializable {
    AGGREGATE,
    RESULT;

    public static StartTime get(long start) {
        return new StartTime(start);
    }

    public static String get(String string) {
        return string;
    }

    public static Record get(int key, double value) {
        return new Record(key, value);
    }

    static class StartTime implements Serializable {
        private final long start;

        public StartTime(long start) {
            this.start = start;
        }

        public long getStart() {
            return start;
        }
    }

    static class Record implements Serializable {
        private final int key;
        private final double value;

        public Record(int key, double value) {
            this.key = key;
            this.value = value;
        }

        public int getKey() {
            return key;
        }

        public double getValue() {
            return value;
        }
    }
}
