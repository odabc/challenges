package ua.pp.odabc.actor;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.RoundRobinPool;
import ua.pp.odabc.ConsoleHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


public class Master extends UntypedActor {
    private final String inFileName;
    private final ActorRef listener;
    private final ActorRef workerRouter;

    public Master(String inFileName, String outFileName, int nrOfWorkers, int nrOfRecords) {
        this.inFileName = inFileName;

        listener = this.getContext().actorOf(
                Props.create(Listener.class, outFileName, nrOfRecords), "listener");
        workerRouter = this.getContext().actorOf(
                new RoundRobinPool(nrOfWorkers).props(Props.create(Worker.class, listener)), "workerRouter");
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (Message.AGGREGATE.equals(message)) {
            ConsoleHelper.infoCalculate();
            listener.tell(Message.get(System.currentTimeMillis()), getSelf());
            try (BufferedReader in = Files.newBufferedReader(Paths.get(inFileName))) {
                while (in.ready()) {
                    workerRouter.tell(Message.get(in.readLine()), getSelf());
                }
            } catch (IOException ignored) {
                ConsoleHelper.message("Something wrong with input file.");
                getContext().system().terminate();
            }
        } else {
            unhandled(message);
        }
    }
}
