package ua.pp.odabc.actor;

import akka.actor.UntypedActor;
import ua.pp.odabc.ConsoleHelper;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public class Listener extends UntypedActor {
    private final String outFileName;
    private int nrOfRecords;
    private long start;
    private Map<Integer, Double> result = new HashMap<>();

    public Listener(String outFileName, int nrOfRecords) {
        this.outFileName = outFileName;
        this.nrOfRecords = nrOfRecords;
    }

    public void onReceive(Object message) {
        if (message instanceof Message.StartTime) {
            start = ((Message.StartTime) message).getStart();
        } else if (message instanceof Message.Record) {
            int key = ((Message.Record) message).getKey();
            double value = ((Message.Record) message).getValue();
            if (result.containsKey(key)) {
                result.put(key, result.get(key) + value);
            } else {
                result.put(key, value);
            }
            nrOfRecords--;
            if (nrOfRecords == 0) {
                getSelf().tell(Message.RESULT, getSelf());
            }
        } else if (Message.RESULT.equals(message)) {
            double total = 0;
            try (BufferedWriter out = Files.newBufferedWriter(Paths.get(outFileName))) {
                for (Map.Entry<Integer, Double> pair : result.entrySet()) {
                    total += pair.getValue();
                    String record = String.format("%d;%.2f%s", pair.getKey(), pair.getValue(), System.lineSeparator());
                    out.write(record);
                }
            } catch (IOException ignored) {
                ConsoleHelper.message("Something wrong with output file.");
                getContext().stop(getSelf());
            }
            Duration duration = Duration.ofMillis(System.currentTimeMillis() - start);
            ConsoleHelper.infoTime(duration);
            ConsoleHelper.infoTotal(total);

            getContext().system().terminate();
        } else {
            unhandled(message);
        }
    }
}
