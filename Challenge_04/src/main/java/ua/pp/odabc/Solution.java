package ua.pp.odabc;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import ua.pp.odabc.actor.Master;
import ua.pp.odabc.actor.Message;

import java.io.*;
import java.nio.file.*;
import java.time.Duration;
import java.util.*;

/**
 *
 */
public class Solution {
    public static void main(String[] args) throws IOException {
        Locale.setDefault(Locale.ENGLISH);

        String inFileName = "records.dat";
        String outFileName = "result.dat";
        int maxId = 1_000;
        int nrOfWorkers = 1_000;
        int nrOfRecords = 100_000;

        createRandomizedRecords(inFileName, maxId, nrOfWorkers, nrOfRecords);
        bruteAggregatorTest(inFileName, "b_" + outFileName);
        akkaAggregatorTest(inFileName, "a_" + outFileName, nrOfWorkers, nrOfRecords);
    }

    public static void createRandomizedRecords(String fileName, int maxId, int nrOfWorkers, int nrOfRecords) {
        ConsoleHelper.infoGenerate();
        try (BufferedWriter out = Files.newBufferedWriter(Paths.get(fileName))) {
            Set<Integer> buf = new HashSet<>();
            int count = 0;
            while (count < nrOfWorkers) {
                int id = (int) Math.floor(maxId * Math.random());
                if (!buf.contains(id)) {
                    buf.add(id);
                    count++;
                }
            }
            Integer[] setID = new Integer[buf.size()];
            buf.toArray(setID);

            for (int i = 0; i < nrOfRecords; i++) {
                int id = setID[(int) Math.floor(setID.length * Math.random())];
                double amount = Math.floor(100_000 * Math.random()) / 100.0;
                String record = String.format("%d;%.2f%s", id, amount, System.lineSeparator());
                out.write(record);
            }
        } catch (IOException ignored) {
            ConsoleHelper.message("Something wrong.");
            System.exit(-1);
        }
    }

    public static void bruteAggregatorTest(String inFileName, String outFileName) {
        Map<Integer, Double> result = new HashMap<>();

        ConsoleHelper.infoCalculate();
        long start = System.currentTimeMillis();
        try (BufferedReader in = Files.newBufferedReader(Paths.get(inFileName))) {
            while (in.ready()) {
                String[] buf = in.readLine().split(";");
                int key = Integer.parseInt(buf[0]);
                double value = Double.parseDouble(buf[1]);
                if (result.containsKey(key)) {
                    result.put(key, result.get(key) + value);
                } else {
                    result.put(key, value);
                }
            }
        } catch (IOException ignored) {
            ConsoleHelper.message("Something wrong with input file.");
            System.exit(-1);
        }

        double total = 0;
        try (BufferedWriter out = Files.newBufferedWriter(Paths.get(outFileName))) {
            for (Map.Entry<Integer, Double> pair : result.entrySet()) {
                total += pair.getValue();
                String record = String.format("%d;%.2f%s", pair.getKey(), pair.getValue(), System.lineSeparator());
                out.write(record);
            }
        } catch (IOException ignored) {
            ConsoleHelper.message("Something wrong with output file.");
            System.exit(-1);
        }
        Duration duration = Duration.ofMillis(System.currentTimeMillis() - start);
        ConsoleHelper.infoTime(duration);
        ConsoleHelper.infoTotal(total);
    }

    public static void akkaAggregatorTest(String inFileName, String outFileName, int nrOfWorkers, int nrOfRecords) {
        ActorSystem system = ActorSystem.create("aggregator");

        final ActorRef master = system.actorOf(
                Props.create(Master.class, inFileName, outFileName, nrOfWorkers, nrOfRecords), "master");
        master.tell(Message.AGGREGATE, ActorRef.noSender());
    }
}