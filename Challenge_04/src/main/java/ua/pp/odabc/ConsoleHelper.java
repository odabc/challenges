package ua.pp.odabc;

import java.time.Duration;

/**
 *
 */
public class ConsoleHelper {
    public static void message(String... strings) {
        for (String string : strings)
            System.out.println(string);
    }

    public static void infoGenerate() {
        ConsoleHelper.message("Process generation randomized data for test");
    }

    public static void infoCalculate() {
        ConsoleHelper.message("Process calculation");
    }

    public static void infoTime(Duration duration) {
        ConsoleHelper.message(String.format("Calculation time: %s", duration.toMillis()));
    }

    public static void infoTotal(Double total) {
        ConsoleHelper.message(String.format("Total value: %.2f", total));
    }
}
