package ua.pp.odabc.search;

import ua.pp.odabc.entity.PathNode;

import java.io.File;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

public class Searcher extends Thread {
    private String rootPath;
    private int depth;
    private Deque<PathNode> searchQueue;

    protected Searcher() {

    }

    public Searcher(String rootPath) {
        setRootPath(rootPath);
        searchQueue = new LinkedList<>();
        searchQueue.add(new PathNode(rootPath, "", 0));
    }

    protected void setRootPath(String rootPath) {
        if (rootPath == null)
            throw new NullPointerException("rootPath must be not null");
        this.rootPath = rootPath;
    }

    protected String getRootPath() {
        return rootPath;
    }

    public void setDepth(int depth) {
        if (depth < 0)
            throw new IllegalArgumentException("depth must be >= 0");
        this.depth = depth;
    }

    protected void setSearchQueue(Deque<PathNode> searchQueue) {
        this.searchQueue = searchQueue;
    }

    protected void incrementNodes() {
        if (!searchQueue.isEmpty()) {
            PathNode pathNode = searchQueue.pollFirst();
            File[] files = new File(pathNode.getPath()).listFiles();
            if (files != null)
                Arrays.stream(files)
                        .filter(file -> file != null)
                        .forEach(file -> searchQueue.addFirst(
                                        new PathNode(file.getPath(), file.getName(), pathNode.getDepth() + 1))
                        );

            System.out.println("increment: \t" + pathNode.getDepth() + "\t" + pathNode.getPath());
        }
    }

    protected Deque<String> processNodes() {
        Deque<String> result = new LinkedList<>();

        while (!searchQueue.isEmpty() && searchQueue.peekFirst().getDepth() == depth) {
            PathNode pathNode = searchQueue.pollFirst();
            result.add(pathNode.getName());

            System.out.println("process: \t" + pathNode.getDepth() + "\t" + pathNode.getPath());
        }

        return result;
    }

    public void search(String mask) {
        while (!isStopped()) {
            processNodes().stream()
                    .filter(s -> s.contains(mask))
                    .forEach(System.out::println);
            incrementNodes();
        }
    }

    public boolean isStopped() {
        return searchQueue.isEmpty();
    }

}
