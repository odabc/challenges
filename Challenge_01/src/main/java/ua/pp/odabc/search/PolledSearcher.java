package ua.pp.odabc.search;

import ua.pp.odabc.entity.PathNode;
import ua.pp.odabc.entity.Pollable;

import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.Exchanger;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class PolledSearcher extends Searcher implements Pollable<String> {
    private Exchanger<String> exchanger;

    protected PolledSearcher() {
    }

    public PolledSearcher(Exchanger<String> exchanger, String rootPath) {
        super(rootPath);
        setExchanger(exchanger);
    }

    protected void setExchanger(Exchanger<String> exchanger) {
        if (exchanger == null)
            throw new NullPointerException("exchanger must be not null");
        this.exchanger = exchanger;
    }

    protected void init() {
        String request = poll(null);

        if (request != null) {
            try {
                setDepth(Integer.parseInt(request));
            } catch (NumberFormatException e) {
                poll(null);
                throw new IllegalArgumentException("Receive bad request.");
            }
        } else {
            throw new NullPointerException("Receive null.");
        }

        Deque<PathNode> searchQueue = new LinkedList<>();
        searchQueue.add(new PathNode(getRootPath(), "", 0));
        setSearchQueue(searchQueue);
    }

    public String poll(String request) {
        try {
            return exchanger.exchange(request, 2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            System.err.println("Interrupted.");
            e.printStackTrace();
        } catch (TimeoutException e) {
            System.err.println("Too long answer.");
        }
        return null;
    }

    @Override
    public void search(String mask) {
        while (!isStopped()) {
            processNodes().forEach(this::poll);
            incrementNodes();
        }
        poll(null);
    }

    @Override
    public void run() {
        init();
        search("");
    }
}
