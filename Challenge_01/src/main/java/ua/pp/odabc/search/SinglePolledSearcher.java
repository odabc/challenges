package ua.pp.odabc.search;

import ua.pp.odabc.entity.PathNode;
import ua.pp.odabc.entity.PrinterNode;

import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Exchanger;

public class SinglePolledSearcher extends PolledSearcher {
    private volatile static SinglePolledSearcher ourSearcher = new SinglePolledSearcher();
    private volatile ConcurrentMap<Exchanger<String>, PrinterNode> queues;
    private boolean isStopped;

    public static SinglePolledSearcher getSearcher() {
        return ourSearcher;
    }

    private SinglePolledSearcher() {
        queues  = new ConcurrentHashMap<>();
        isStopped = false;
        setDaemon(true);
    }

    @Override
    public void setRootPath(String rootPath) {
        super.setRootPath(rootPath);
    }

//    @Override
//    public boolean isStopped() {
//        return isStopped;
//    }
//
//    public boolean isStopped(boolean isStopped) {
//        this.isStopped = isStopped;
//        return isStopped;
//    }

    public void addPrinter(Exchanger<String> exchanger, int depth) {
        if (exchanger == null)
            throw new NullPointerException("exchanger must be not null");
        if (depth < 0)
            throw new IllegalArgumentException("depth must be >= 0");

        Deque<PathNode> root = new LinkedList<>();
        root.add(new PathNode(getRootPath(), "", 0));
        queues.put(exchanger, new PrinterNode(depth, root));
    }

    protected void switchSearcher(ConcurrentMap.Entry<Exchanger<String>, PrinterNode> printer) {
        Exchanger<String> exchanger = printer.getKey();
        PrinterNode printerNode = printer.getValue();

        setExchanger(exchanger);
        setDepth(printerNode.getDepth());
        setSearchQueue(printerNode.getSearchQueue());
    }

    protected void processPrinter(ConcurrentMap.Entry<Exchanger<String>, PrinterNode> printer) {
        switchSearcher(printer);

        if (!printer.getValue().getSearchQueue().isEmpty()) {
            processNodes().forEach(this::poll);
            incrementNodes();
        } else {
            poll(null);
            removePrinter(printer.getKey());
        }
    }

    protected void removePrinter(Exchanger<String> exchanger) {
        queues.remove(exchanger);
    }

    @Override
    public void run() {
        // stop when shutdown server
        while (!interrupted() && !isStopped) {
            queues.entrySet().forEach(this::processPrinter);
        }
    }
}

