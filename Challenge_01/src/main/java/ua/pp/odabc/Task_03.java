package ua.pp.odabc;
/**
 * Modify the application of the objective 1, 2 in a simple multiplayer telnet server:
 * the application takes two parameters:
 * - ServerPort - the port that it will "listen"
 * - Path to the starting directory (rootPath)
 * <p>
 * Search criteria (depth, and mask) are set through the console client telnet
 * (Use for this the standard program: telnet, putty, ...)
 * For example, the user enters:
 * <p>
 * 2 ab
 * 2 - Depth
 * ab - Mask
 * <p>
 * Requirements:
 * <p>
 * - * All * calls to the file system should be made from a single thread * *
 * {
 * Ie It exists on the server thread, from which it only accesses the file system.
 * }
 * <p>
 * - "Telnet server" to be multi-user interactive +
 * {
 * If the server will drop at the same time and each 4 clients ask "search terms",
 * the results to clients must come in parallel, rather than sequentially,
 * ie the user does not have to wait for the completion of delivery of results over the previous users.
 * }
 */

import ua.pp.odabc.server.TelnetServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task_03 {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Must use with arguments: port rootPath");
            return;
        }

        try {
            int port = Integer.parseInt(args[0]);
            if (port < 0) {
                System.err.println("Port must be positive!");
                return;
            }

            try (TelnetServer serverPool = new TelnetServer(port, args[1]);
                 BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {

                serverPool.start();

                String input = "";
                while (!"exit".equals(input)) {
                    input = reader.readLine();
                }
            } catch (IOException | IllegalStateException e) {
                System.err.println(e.getMessage());
            }

        } catch (NumberFormatException e) {
            System.err.println("Port must be integer!");
        }
    }
}
