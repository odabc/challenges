package ua.pp.odabc;
/**
Write a console application in Java, which takes three parameters:
     - Path to the root directory (rootPath)
     - The depth of the search - a non-negative integer (depth)
     - Mask - line (mask)

The application must find all the elements of the file system tree located on
the depth of the root depth rootPath, which in its name contains the string mask.

Requirements:
     - The application must be implemented without recursion.
     - The application should not depend on the operating system.
*/

import ua.pp.odabc.search.Searcher;

public class Task_01 {
    public static void main(String[] args) {
        int numArgs = args.length;
        if (numArgs < 1) {
            System.err.println("Must use with arguments: rootPath [depth [mask]]");
            return;
        }
        Searcher searcher = new Searcher(args[0]);

        try {
            searcher.setDepth((numArgs < 2) ? 0 : Integer.parseInt(args[1]));
        } catch (NumberFormatException e) {
            System.err.println("Depth must be positive integer!");
            return;
        }

        String mask = numArgs < 3 ? "" : args[2];
        searcher.search(mask);
    }
}
