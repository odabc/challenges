package ua.pp.odabc.server;

import ua.pp.odabc.print.TelnetPolledPrinter;
import ua.pp.odabc.search.SinglePolledSearcher;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Exchanger;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TelnetServer extends Thread implements AutoCloseable {
    private boolean isStopped;
    private ServerSocket server;
    private ExecutorService threadPool;

    public TelnetServer(int port, String rootPath) {
        isStopped = false;
        threadPool = Executors.newFixedThreadPool(10);

        init(port, rootPath);
    }

    private void init(int port, String rootPath) {
        try {
            server = new ServerSocket(port);
            System.out.println("Server started work ...");
        } catch (IllegalArgumentException | IOException e) {
            threadPool.shutdown();
            throw new IllegalStateException("Couldn't connect to port " + port);
        }

        // start searcher thread
        SinglePolledSearcher.getSearcher().setRootPath(rootPath);
        SinglePolledSearcher.getSearcher().start();
    }

    public void run() {
        while (!isStopped) {
            try {
                Socket client = server.accept();
                Exchanger<String> exchanger = new Exchanger<>();

                threadPool.execute(
                        new TelnetPolledPrinter(client.getInputStream(), client.getOutputStream(), exchanger));
                System.out.println("New connection ...");
            } catch (IOException e) {
                System.err.println("Error while opening new connection or server is shutting down.");
            }
        }
    }

    @Override
    public void close() {
        isStopped = true;
        System.out.println("Shutting down ...");
        try {
            server.close();
        } catch (IOException e) {
            System.err.println("Error while closing server.");
        } finally {
            threadPool.shutdown();
//            threadPool.shutdownNow();

            // close searcher thread
//            SinglePolledSearcher.getSearcher().isStopped(true);

            System.out.println("Server stopped.");
            System.out.println("Wait until all connections will be closed.");
        }
    }
}
