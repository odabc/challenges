package ua.pp.odabc;

import ua.pp.odabc.print.PolledPrinter;
import ua.pp.odabc.search.PolledSearcher;

import java.util.concurrent.Exchanger;

public class Task_02 {
    public static void main(String[] args) {
        int numArgs = args.length;
        if (numArgs < 1) {
            System.err.println("Must use with arguments: rootPath [depth [mask]]");
        } else {
            int depth;
            try {
                depth = numArgs < 2 ? 0 : Integer.parseInt(args[1]);
                if (depth < 0)
                        throw new NumberFormatException("For input string: \"" + args[1] + "\"");
            } catch (NumberFormatException e) {
                System.err.println(e.getMessage() + " - depth must be positive integer");
                return;
            }

            String mask = numArgs < 3 ? "" : args[2];

            Exchanger<String> exchanger = new Exchanger<>();

            new PolledSearcher(exchanger, args[0]).start();
            new PolledPrinter(exchanger, depth, mask).start();
        }
    }
}
