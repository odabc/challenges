package ua.pp.odabc.print;

import ua.pp.odabc.entity.Pollable;

import java.util.concurrent.Exchanger;

public class PolledPrinter extends Thread implements Pollable<String> {
    private final Exchanger<String> exchanger;
    private int depth;
    private String mask;

    public PolledPrinter(Exchanger<String> exchanger) {
        if (exchanger == null)
            throw new NullPointerException("exchanger must be not null");
        this.exchanger = exchanger;
    }

    public PolledPrinter(Exchanger<String> exchanger, int depth, String mask) {
        if (exchanger == null)
            throw new NullPointerException("exchanger must be not null");
        this.exchanger = exchanger;
        setDepthMask(depth, mask);
    }

    protected Exchanger<String> getExchanger() {
        return exchanger;
    }

    protected void setDepthMask(int depth, String mask) {
        if (depth < 0)
            throw new IllegalArgumentException("depth must be >= 0");
        if (mask == null)
            throw new NullPointerException("mask must be not null");
        this.depth = depth;
        this.mask = mask;
    }

    public String poll(String request) {
        try {
            return exchanger.exchange(request);
        } catch (InterruptedException e) {
            System.err.println("Interrupted.");
        }
        return null;
    }

    public void run() {
        poll(Integer.toString(depth));

        String result = poll(null);

        while (result != null) {
            if (result.contains(mask))
                System.out.println(result);
            result = poll(null);
        }

    }
}
