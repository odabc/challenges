package ua.pp.odabc.print;

import ua.pp.odabc.search.SinglePolledSearcher;

import java.io.*;
import java.util.concurrent.Exchanger;

public class TelnetPolledPrinter extends PolledPrinter {
    private BufferedReader in;
    private PrintStream out;

    public TelnetPolledPrinter(InputStream is, OutputStream os, Exchanger<String> exchanger) {
        super(exchanger);
        in = new BufferedReader(new InputStreamReader(is));
        out = new PrintStream(os, true);
    }

    @Override
    public void run() {
        out.println("Wait for request");
        String reader;
        SinglePolledSearcher searcher = SinglePolledSearcher.getSearcher();

        try (BufferedReader in = this.in; PrintStream out = this.out) {
            while (!interrupted() && (reader = in.readLine()) != null && !"exit".equalsIgnoreCase(reader)) {
                try {
                    String[] request = reader.split(" ");
                    int depth = Integer.parseInt(request[0]);
                    String mask = (request.length < 2) ? "" : request[1];

                    setDepthMask(depth, mask);
                    searcher.addPrinter(getExchanger(), depth);

                    String result = poll(null);

                    while (result != null) {
                        if (result.contains(mask))
                            out.println(result);
                        result = poll(null);
                    }
                } catch (NumberFormatException e) {
                    out.println("depth must be integer");
                    out.println("Please type: depth [mask] or exit");
                } catch (IllegalArgumentException | NullPointerException e) {
                    out.println(e.getMessage());
                    out.println("Please type: depth [mask] or exit");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
