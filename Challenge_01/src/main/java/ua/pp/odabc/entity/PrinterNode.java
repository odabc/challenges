package ua.pp.odabc.entity;

import java.util.Deque;

public class PrinterNode {
    final Deque<PathNode> searchQueue;
    final int depth;

    public PrinterNode(int depth, Deque<PathNode> searchQueue) {
        this.depth = depth;
        this.searchQueue = searchQueue;
    }

    public Deque<PathNode> getSearchQueue() {
        return searchQueue;
    }

    public int getDepth() {
        return depth;
    }
}
