package ua.pp.odabc.entity;

public interface Pollable<T> {
    T poll(T request);
}
