package ua.pp.odabc.entity;

public class PathNode {
    private final String path;
    private final String name;
    private final int depth;

    public PathNode(String path, String name, int depth) {
        this.path = path;
        this.name = name;
        this.depth = depth;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public int getDepth() {
        return depth;
    }
}
