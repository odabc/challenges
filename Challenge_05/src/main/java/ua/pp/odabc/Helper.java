package ua.pp.odabc;

import org.opencv.core.Core;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Helper {

    public static void loadLibrary() {
        String libraryName = Core.NATIVE_LIBRARY_NAME + ".dll";
        copyIfAbsent(libraryName);
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    public static void checkDll(String dllName) {
        copyIfAbsent(dllName);
    }

    private static void copyIfAbsent(String name) {
        Path path = Paths.get(name);
        if (!Files.isReadable(path)) {
            try (BufferedInputStream in = new BufferedInputStream(
                    Helper.class.getClassLoader().getResourceAsStream("lib/" + name))) {
                Files.copy(in, path);
                path.toFile().deleteOnExit();
            } catch (IOException ignored) {
            }
        }
    }

    public static void ffmpeg(String[] args) {
        ProcessBuilder pb = new ProcessBuilder(args);
        try {
            final Process p = pb.start();
        } catch (IOException ignored) {
        }
    }

    public static void loadCascades(String name) {
        try (BufferedInputStream in = new BufferedInputStream(
                Helper.class.getClassLoader().getResourceAsStream("Cascades/" + name))){
            // load cascade file from application resources
//            InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
//            File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
//            mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
//            FileOutputStream os = new FileOutputStream(mCascadeFile);
//
//            byte[] buffer = new byte[4096];
//            int bytesRead;
//            while ((bytesRead = is.read(buffer)) != -1) {
//                os.write(buffer, 0, bytesRead);
//            }
//            is.close();
//            os.close();
        } catch (IOException ignored) {
        }
    }
}
