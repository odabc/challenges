package ua.pp.odabc;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;
import org.opencv.video.BackgroundSubtractor;
import org.opencv.video.Video;
import org.opencv.videoio.VideoCapture;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * The controller for our application, where the application logic is
 * implemented. It handles the button for starting/stopping the camera and the
 * acquired video stream.
 */
public class MainController {
    // the FXML button
    @FXML
    private Button button;
    // the FXML image view
    @FXML
    private ImageView currentFrame;

    // a timer for acquiring the video stream
    private ScheduledExecutorService timer;
    // the OpenCV object that realizes the video capture
    private VideoCapture capture = new VideoCapture();
    // a flag to change the button behavior
    private boolean cameraActive = false;

    private BackgroundSubtractor backgroundSubtractor = Video.createBackgroundSubtractorKNN();
    private int num = 0;

    private CascadeClassifier faceCascade =
            new CascadeClassifier("D:/Java Work/Challenges/Challenge_05/src/main/resources/cascades/haarcascade_fullbody.xml");
    private int absoluteFaceSize = 0;

    /**
     * The action triggered by pushing the button on the GUI
     *
     * @param event the push button event
     */
    @FXML
    protected void startCamera(ActionEvent event) {
        if (!this.cameraActive) {
            // start the video capture
//            this.capture.open("rtmp://odabc.pp.ua/vod/test.mp4");
            this.capture.open("rtmp://cdn.ua/bukovel.com_camera/_definst_/08_720p.stream");

            // is the video stream available?
            if (this.capture.isOpened()) {
                this.cameraActive = true;

                // grab a frame every 33 ms (30 frames/sec)
                Runnable frameGrabber = new Runnable() {
                    @Override
                    public void run() {
                        Image imageToShow = grabFrame();
                        currentFrame.setImage(imageToShow);
                    }
                };

                this.timer = Executors.newSingleThreadScheduledExecutor();
                this.timer.scheduleAtFixedRate(frameGrabber, 0, 33, TimeUnit.MILLISECONDS);

                // update the button content
                this.button.setText("Stop Camera");
            } else {
                // log the error
                System.err.println("Impossible to open the camera connection...");
            }
        } else {
            // the camera is not active at this point
            this.cameraActive = false;
            // update again the button content
            this.button.setText("Start Camera");

            // stop the timer
            try {
                this.timer.shutdown();
                this.timer.awaitTermination(33, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                // log the exception
                System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
            }

            // release the camera
            this.capture.release();
            // clean the frame
            this.currentFrame.setImage(null);
        }
    }

    /**
     * Get a frame from the opened video stream (if any)
     *
     * @return the {@link Image} to show
     */
    private Image grabFrame() {
        // init everything
        Image imageToShow = null;
        Mat frame = new Mat();

        // check if the capture is open
        if (this.capture.isOpened()) {
            try {
                // read the current frame
                this.capture.read(frame);

                // if the frame is not empty, process it
                if (!frame.empty()) {
                    Imgproc.resize(frame, frame, new Size(640, 480));
                    Mat grayFrame = new Mat();

                    // convert the frame in gray scale
                    Imgproc.cvtColor(frame, grayFrame, Imgproc.COLOR_BGR2GRAY);

/*
                    // remove some noise
                    Imgproc.GaussianBlur(grayFrame, grayFrame, new Size(3.0, 3.0), Imgproc.CV_GAUSSIAN);
                    Imgproc.medianBlur(grayFrame, grayFrame, 3);

                    this.backgroundSubtractor.apply(grayFrame, grayFrame, 0.005);

                    Core.inRange(grayFrame, new Scalar(196), new Scalar(255), grayFrame);

                    int size = 16;
                    Mat element = Imgproc.getStructuringElement(2, new Size(size*2+1, size*2+1), new Point(size, size));
                    Imgproc.dilate(grayFrame, grayFrame, element);
                    Imgproc.erode(grayFrame, grayFrame, element);

                    // init
                    List<MatOfPoint> contours = new ArrayList<>();
                    Mat hierarchy = new Mat();

                    // find contours
                    Imgproc.findContours(grayFrame, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE);

                    // if any contour exist...
                    if (hierarchy.size().height > 0 && hierarchy.size().width > 0) {
                        // for each contour, display it in blue
                        for (int i = 0; i < contours.size(); i++) {
                            MatOfPoint currentContour = contours.get(i);
                            double currentArea = Imgproc.contourArea(currentContour);
                            if (currentArea > 10) {
                                Imgproc.drawContours(frame, contours, i, new Scalar(0, 255, 0), 2);
                                Rect rectangle = Imgproc.boundingRect(currentContour);
                                Imgproc.rectangle(frame, rectangle.tl(), rectangle.br(), new Scalar(255, 0, 0), 1);

                                float[] radius = new float[1];
                                Point center = new Point();
                                MatOfPoint2f currentContour2f = new MatOfPoint2f();
                                currentContour.convertTo(currentContour2f, CvType.CV_32FC2);
                                Imgproc.minEnclosingCircle(currentContour2f, center, radius);
                                Imgproc.circle(frame, center, 2, new Scalar(0, 0, 255), 2);
                            }

                        }
                    }
*/


                    MatOfRect objects = new MatOfRect();

                    // equalize the frame histogram to improve the result
                    Imgproc.equalizeHist(grayFrame, grayFrame);

                    // compute minimum face size (20% of the frame height, in our case)
                    if (absoluteFaceSize == 0) {
                        int height = grayFrame.rows();
                        if (Math.round(height * 0.2f) > 0) {
                            absoluteFaceSize = Math.round(height * 0.2f);
                        }
                    }

                    // detect objects
                    faceCascade.detectMultiScale(grayFrame, objects, 1.1, 0, Objdetect.CASCADE_SCALE_IMAGE,
                            new Size(0.5*absoluteFaceSize, absoluteFaceSize), new Size());
//                    faceCascade.detectMultiScale(grayFrame, objects);

                    // each rectangle in objects is a face: draw them!
                    Rect[] facesArray = objects.toArray();
                    for (int i = 0; i < facesArray.length; i++) {
                        Point point = new Point(facesArray[i].x + facesArray[i].width / 2,
                                facesArray[i].y + facesArray[i].height / 2);
                        Imgproc.circle(frame, point, 2, new Scalar(0, 0, 255), 2);
                        Imgproc.rectangle(frame, facesArray[i].tl(), facesArray[i].br(), new Scalar(0, 255, 0), 1);
                    }


                    Imgproc.line(frame,
                            new Point(0, frame.height() * 0.85),
                            new Point(frame.width(), frame.height() * 0.85),
                            new Scalar(0, 0, 255), 1);

                    // convert the Mat object (OpenCV) to Image (JavaFX)
//                    Imgproc.resize(frame, frame, new Size(640, 480));
                    imageToShow = mat2Image(frame);
                }

            } catch (Exception e) {
                // log the error
                System.err.println("Exception during the image elaboration: " + e);
            }
        }

        return imageToShow;
    }

    /**
     * Convert a Mat object (OpenCV) in the corresponding Image for JavaFX
     *
     * @param frame the {@link Mat} representing the current frame
     * @return the {@link Image} to show
     */
    private Image mat2Image(Mat frame) {
        // create a temporary buffer
        MatOfByte buffer = new MatOfByte();
//        Imgcodecs.imwrite(String.format("d:/out/%04d.jpg", num++), frame);

        // encode the frame in the buffer
        Imgcodecs.imencode(".png", frame, buffer);
        // build and return an Image created from the image encoded in the
        // buffer
        return new Image(new ByteArrayInputStream(buffer.toArray()));
    }
}
