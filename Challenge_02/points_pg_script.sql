--ID | FIELD_NAME | XX | YY
DROP TABLE IF EXISTS ossystem.points;
DROP SCHEMA IF EXISTS ossystem;

CREATE SCHEMA ossystem;
CREATE TABLE ossystem.points (
  id         SERIAL       PRIMARY KEY ,
  field_name VARCHAR(100) NOT NULL DEFAULT '',
  xx         INTEGER      NOT NULL DEFAULT '0',
  yy         INTEGER      NOT NULL DEFAULT '0'
);

--
-- Dumping data for table points
--
SET SEARCH_PATH TO ossystem;
DELETE FROM points;
INSERT INTO points (field_name, xx, yy) VALUES ('Point Y1',   0,   0);
INSERT INTO points (field_name, xx, yy) VALUES ('Point Y2',  60, 100);
INSERT INTO points (field_name, xx, yy) VALUES ('Point Y3',   0, 200);
INSERT INTO points (field_name, xx, yy) VALUES ('Point Y4',  60, 100);
INSERT INTO points (field_name, xx, yy) VALUES ('Point Y5', 120, 200);
INSERT INTO points (field_name, xx, yy) VALUES ('Point R1', 140, 200);
INSERT INTO points (field_name, xx, yy) VALUES ('Point R2', 140, 100);
INSERT INTO points (field_name, xx, yy) VALUES ('Point R3', 140, 200);
INSERT INTO points (field_name, xx, yy) VALUES ('Point R4', 200, 200);
INSERT INTO points (field_name, xx, yy) VALUES ('Point R5', 240, 170);
INSERT INTO points (field_name, xx, yy) VALUES ('Point R6', 240, 130);
INSERT INTO points (field_name, xx, yy) VALUES ('Point R7', 200, 100);
INSERT INTO points (field_name, xx, yy) VALUES ('Point R8', 140, 100);
INSERT INTO points (field_name, xx, yy) VALUES ('Point R9', 140,   0);
INSERT INTO points (field_name, xx, yy) VALUES ('Point A1', 250,   0);
INSERT INTO points (field_name, xx, yy) VALUES ('Point A2', 310, 200);
INSERT INTO points (field_name, xx, yy) VALUES ('Point A3', 380,   0);
INSERT INTO points (field_name, xx, yy) VALUES ('Point A4', 345, 100);
INSERT INTO points (field_name, xx, yy) VALUES ('Point A5', 280, 100);
COMMIT;

                                                                                                                                                                                                                                                                 
