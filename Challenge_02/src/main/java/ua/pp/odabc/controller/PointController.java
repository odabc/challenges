package ua.pp.odabc.controller;

import ua.pp.odabc.entity.Point;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;

public interface PointController {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    Collection<Point> getQuerySearchPoints(@QueryParam("search") String search);

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response putPathIdPoint(@PathParam("id") int id, Point point);

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    Response deletePathIdPoint(@PathParam("id") int id);

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response postNewPoint(Point point);

    @GET
    @Path("verify")
    @Produces(MediaType.TEXT_PLAIN)
    Response verifyRESTService();
}
