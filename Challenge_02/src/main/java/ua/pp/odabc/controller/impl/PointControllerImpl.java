package ua.pp.odabc.controller.impl;

import ua.pp.odabc.entity.Point;
import ua.pp.odabc.service.PointService;
import ua.pp.odabc.service.impl.PointServiceImpl;
import ua.pp.odabc.controller.PointController;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.Collection;

@Path("points")
public class PointControllerImpl implements PointController {
    private PointService service = new PointServiceImpl();

    public Collection<Point> getQuerySearchPoints(String search) {
        return service.get(search == null ? "" : search);
    }

    public Response putPathIdPoint(int id, Point point) {
        service.update(point);
        return Response.ok().build();
    }

    public Response deletePathIdPoint(int id) {
        service.delete(id);
        return Response.ok().build();
    }

    public Response postNewPoint(Point point) {
        point = service.add(point);
        return Response.ok().entity(point).build();
    }

    public Response verifyRESTService() {
        StringBuilder result = new StringBuilder();

        result.append("RESTService Successfully started.");
        result.append(System.lineSeparator());
        for (Point point : service.getAll()) {
            result.append(point.toString());
            result.append(System.lineSeparator());
        }

        // return HTTP response 200 in case of success
        return Response.status(200).entity(result.toString()).build();
    }
}

