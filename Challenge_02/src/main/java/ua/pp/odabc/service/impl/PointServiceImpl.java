package ua.pp.odabc.service.impl;

import ua.pp.odabc.service.PointService;
import ua.pp.odabc.entity.Point;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;

public class PointServiceImpl implements PointService {

    public Point add(Point point) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        em.getTransaction().begin();
        Point pointFromDB = em.merge(point);
        em.getTransaction().commit();
        em.close();
        return pointFromDB;
    }

    public Point get(int id) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        Point result = em.find(Point.class, id);
        em.close();
        return result;
    }

    public void delete(int id) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        em.getTransaction().begin();
        em.remove(get(id));
        em.getTransaction().commit();
        em.close();
    }

    public void delete(Point point) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        em.getTransaction().begin();
        em.remove(point);
        em.getTransaction().commit();
        em.close();
    }

    public void deleteAll() {
        for (Point point : getAll())
            delete(point.getID());
    }

    public void update(Point point) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        em.getTransaction().begin();
        em.merge(point);
        em.getTransaction().commit();
        em.close();
    }

    public Collection<Point> get(String search) {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        TypedQuery<Point> query = em.createQuery(
                "select p from Point p where p.field_name like :search", Point.class)
                .setParameter("search", search.trim() + "%");
        Collection<Point> result = query.getResultList();
        em.close();
        return result;
    }

    public Collection<Point> getAll() {
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        TypedQuery<Point> query = em.createNamedQuery("Point.getAll", Point.class);
        em.close();
        return query.getResultList();
    }
}