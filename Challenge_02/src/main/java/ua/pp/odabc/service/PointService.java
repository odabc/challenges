package ua.pp.odabc.service;

import ua.pp.odabc.entity.Point;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Collection;

public interface PointService {
    EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("OSSYSTEM");

    Point add(Point point);

    void delete(int id);

    void delete(Point point);

    void deleteAll();

    Point get(int id);

    void update(Point point);

    Collection<Point> getAll();

    Collection<Point> get(String search);
}
