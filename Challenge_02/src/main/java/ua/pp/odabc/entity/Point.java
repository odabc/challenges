package ua.pp.odabc.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "points", schema = "ossystem")
@NamedQueries({
        @NamedQuery(name = "Point.getAll", query = "select p from Point p"),
        @NamedQuery(name = "Point.deleteAll", query = "delete from Point p")
})
@XmlRootElement
public class Point implements Serializable {

    @Id
    @XmlElement
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "field_name", length = 100)
    private String field_name;

    @Column(name = "xx")
    private int xx;

    @Column(name = "yy")
    private int yy;

    public Point() {
    }

    public Point(String field_name, int xx, int yy) {
        this.field_name = field_name;
        this.xx = xx;
        this.yy = yy;
    }

    public int getID() {
        return id;
    }

    public String getName() {
        return field_name;
    }

    public void setName(String name) {
        this.field_name = name;
    }

    public int getX() {
        return xx;
    }

    public void setX(int coord) {
        this.xx = coord;
    }

    public int getY() {
        return yy;
    }

    public void setY(int coord) {
        this.yy = coord;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Point)) return false;

        Point point = (Point) o;

        if (id != point.id) return false;
        if (xx != point.xx) return false;
        if (yy != point.yy) return false;
        return field_name.equals(point.field_name);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + field_name.hashCode();
        result = 31 * result + xx;
        result = 31 * result + yy;
        return result;
    }

    @Override
    public String toString() {
        return String.format("Point{id=%d, name='%s\', x=%d, y=%d}", id, field_name, xx, yy);
    }

}