Ext.define('PointApp.view.Viewport', {
    extend: 'Ext.container.Viewport',
    alias: 'widget.viewportView',
    layout: 'fit',
    padding: 2,

    requires: [
        'PointApp.view.PointGridView',
        'PointApp.view.PointSearchView',
        'PointApp.view.PointChartView'
    ],

    initComponent: function () {
        this.items = {
            title: 'Тестовое задание - каталог точек.',
            layout: 'border',
            items: [{
                xtype: 'pointChartView',
                region: 'north',
                height: '60%'
            }, {
                xtype: 'pointGridView',
                region: 'center'
            }, {
                xtype: 'pointSearchView',
                title: 'Выборка по названию',
                region: 'west',
                width: 170
            }]
        };

        this.callParent();
    }
});
