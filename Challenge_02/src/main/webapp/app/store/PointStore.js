Ext.define('PointApp.store.PointStore', {
    extend: 'Ext.data.Store',
    model: 'PointApp.model.Point',
    requires: ['PointApp.model.Point'],
    autoLoad: true,
    autoSync: true,
    proxy: {
        type: 'rest',
        url: 'rest/points',
        //api: {
            //create: 'rest/points',
            //read: 'rest/points',
            //destroy: 'rest/points',
            //update: 'rest/points'
        //},
        reader: {
            type: 'json',
            root: 'point',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});