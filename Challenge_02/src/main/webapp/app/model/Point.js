Ext.define('PointApp.model.Point', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'id', type: 'int'},
        {name: 'name', type: 'string'},
        {name: 'x', type: 'int'},
        {name: 'y', type: 'int'}
    ],
    proxy: {
        type: 'rest',
        url: 'rest/points',
        //api: {
            //create: 'rest/points',
            //read: 'rest/points',
            //destroy: 'rest/points',
            //update: 'rest/points'
        //},
        reader: {
            type: 'json',
            dataProperty: 'point',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            writeAllFields: true
        }
    }
});