Ext.application({
    name: 'PointApp',

    autoCreateViewport: true,

    models: ['Point'],
    stores: ['PointStore'],
    controllers: [
        'PointGridController',
        'PointAddController',
        'PointSearchController'
    ]
});