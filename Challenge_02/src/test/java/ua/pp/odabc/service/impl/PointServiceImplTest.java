package ua.pp.odabc.service.impl;

import ua.pp.odabc.service.PointService;
import ua.pp.odabc.entity.Point;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.assertEquals;

public class PointServiceImplTest {

    PointService service = new PointServiceImpl();

    @AfterClass
    public static void close() {
        PointService service = new PointServiceImpl();
        service.deleteAll();
        service.add(new Point("Point Y1", 0, 0));
        service.add(new Point("Point Y2", 60, 100));
        service.add(new Point("Point Y3", 0, 200));
        service.add(new Point("Point Y4", 60, 100));
        service.add(new Point("Point Y5", 120, 200));
        service.add(new Point("Point R1", 140, 200));
        service.add(new Point("Point R2", 140, 100));
        service.add(new Point("Point R3", 140, 200));
        service.add(new Point("Point R4", 200, 200));
        service.add(new Point("Point R5", 240, 170));
        service.add(new Point("Point R6", 240, 130));
        service.add(new Point("Point R7", 200, 100));
        service.add(new Point("Point R8", 140, 100));
        service.add(new Point("Point R9", 140, 0));
        service.add(new Point("Point A1", 250, 0));
        service.add(new Point("Point A2", 310, 200));
        service.add(new Point("Point A3", 380, 0));
        service.add(new Point("Point A4", 345, 100));
        service.add(new Point("Point A5", 280, 100));
    }

    @Before
    public void init() {
        service.deleteAll();
    }

    @Test
    public void testAddRecord() throws Exception {
        Point point = new Point("Test Saved", 0, 10);

        Point pointFromDB = service.add(point);
        // assert statements
        assertEquals("Name must be same", point.getName(), pointFromDB.getName());
        assertEquals("Coordinate X must be same", point.getX(), pointFromDB.getX());
        assertEquals("Coordinate Y must be same", point.getY(), pointFromDB.getY());
    }

    @Test
    public void testGetRecord() throws Exception {
        Point point = new Point("Test Geted", 250, 1000);
        point = service.add(point);

        Point pointFromDB = service.get(point.getID());
        // assert statements
        assertEquals("ID must be same", point.getID(), pointFromDB.getID());
        assertEquals("Name must be same", point.getName(), pointFromDB.getName());
        assertEquals("Coordinate X must be same", point.getX(), pointFromDB.getX());
        assertEquals("Coordinate Y must be same", point.getY(), pointFromDB.getY());
    }

    @Test
    public void testDeleteRecordByID() throws Exception {
        Point point = new Point("Test Deleted by ID", 100, 10);
        point = service.add(point);

        service.delete(point.getID());
        // assert statements
        assertEquals("Record ID must absent in DB", null, service.get(point.getID()));
    }

    @Test
    public void testDeleteRecordByPoint() throws Exception {
        Point point = new Point("Test Deleted by Point", 2354360, 869708744);
        point = service.add(point);

        service.delete(point);
        // assert statements
        assertEquals("Record ID must absent in DB", null, service.get(point.getID()));
    }

    @Test
    public void testUpdate() throws Exception {
        Point point = new Point("Test Updated", 1233, 78654);

        point = service.add(point);
        point.setName("Test for new Name");
        point.setX(66);
        point.setY(99);

        service.update(point);
        Point pointFromDB = service.get(point.getID());
        // assert statements
        assertEquals("Points must be same", point, pointFromDB);
    }

    @Test
    public void testGet(){
        Point point1 = new Point("Test Get All 1", 123, 333);
        Point point2 = new Point("Test Get All 2", 686, 666);
        Point point3 = new Point("Test Get All 3", 999, 777);

        point1 = service.add(point1);
        point2 = service.add(point2);
        point3 = service.add(point3);

        Collection<Point> points = service.get("Test");
        // assert statements
        assertEquals("Count must be 3", 3, points.size());
        assertEquals("Point must be in Collection", true, points.contains(point1));
        assertEquals("Point must be in Collection", true, points.contains(point2));
        assertEquals("Point must be in Collection", true, points.contains(point3));
    }

    @Test
    public void testGetAll(){
        Point point1 = new Point("Test Get All 1", 123, 333);
        Point point2 = new Point("Test Get All 2", 686, 666);
        Point point3 = new Point("Test Get All 3", 999, 777);

        point1 = service.add(point1);
        point2 = service.add(point2);
        point3 = service.add(point3);

        Collection<Point> points = service.getAll();
        // assert statements
        assertEquals("Count must be 3", 3, points.size());
        assertEquals("Point must be in Collection", true, points.contains(point1));
        assertEquals("Point must be in Collection", true, points.contains(point2));
        assertEquals("Point must be in Collection", true, points.contains(point3));
    }
}