package ua.pp.odabc.dispatcher;

import ua.pp.odabc.aviation.Flier;
import ua.pp.odabc.exception.JChannelNotCreatingException;
import ua.pp.odabc.message.JChannelJson;
import ua.pp.odabc.message.ClusterChannelFactory;

import java.io.IOException;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.*;

public class Dispatcher implements Runnable {
    private final Map<Integer, Flier> flierMap = new ConcurrentHashMap<>();
    private final Logger logger;
    private final JChannelJson channel;
    private volatile boolean isStopped;

    public Dispatcher() throws JChannelNotCreatingException {
        logger = Logger.getLogger(ClusterChannelFactory.getClusterName());

        try {
            LogManager.getLogManager().readConfiguration(
                    Thread.currentThread().getContextClassLoader()
                            .getResourceAsStream("logger.properties"));
        } catch (IOException exception) {
            logger.log(Level.SEVERE, "Error in loading configuration", exception);
        }

        channel = ClusterChannelFactory.getReceiver();
        isStopped = false;
    }

    public void addFlier(Flier flier) {
        if (flierMap.containsKey(flier.getId())) {
            throw new IllegalArgumentException("ID duplicated");
        }
        flierMap.put(flier.getId(), flier);
    }

    public void removeFlier(int id) {
        if (!flierMap.containsKey(id)) {
            throw new IllegalArgumentException("ID absent");
        }
        flierMap.remove(id);
    }

    public void stop() {
        flierMap.values().forEach(Flier::landing);
        isStopped = true;
    }

    @Override
    public void run() {
        while (!isStopped) {
            logReceivedMessage();
            try {
                Thread.sleep(10_000);
            } catch (InterruptedException e) {
                isStopped = true;
            }
        }
        logReceivedMessage();
        channel.close();
    }

    private void logReceivedMessage() {
        Queue<String> msg = channel.getJsonQueue();
        while (!msg.isEmpty()) {
            logger.info(msg.poll());
        }
    }
}
