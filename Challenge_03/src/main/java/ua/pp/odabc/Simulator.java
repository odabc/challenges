package ua.pp.odabc;

import ua.pp.odabc.aviation.Aircraft;
import ua.pp.odabc.aviation.AircraftType;
import ua.pp.odabc.aviation.Helicopter;
import ua.pp.odabc.aviation.HelicopterType;
import ua.pp.odabc.dispatcher.Dispatcher;
import ua.pp.odabc.entity.Course;
import ua.pp.odabc.entity.Location;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 *  Windows issue with setting UDP.ip_ttl in IPv6
 *  https://github.com/belaban/JGroups/wiki/FAQ
 *  -Djava.net.preferIPv4Stack=true
 */
public class Simulator {
    public static void main(String[] args) throws Exception {
        System.setProperty("java.net.preferIPv4Stack" , "true");

        ExecutorService threadPool = Executors.newFixedThreadPool(15);

        // GeoDataSource
        Location odessa = new Location(46.4775, 30.7326, 0);
        Location kiev = new Location(50.4547, 30.5238, 0);

        Dispatcher dispatcher = new Dispatcher();
        threadPool.submit(dispatcher);

        Aircraft a1 = new Aircraft(123, AircraftType.A_300, odessa, new Course(-2.95, 360));
        Aircraft a2 = new Aircraft(456, AircraftType.B_777, kiev, new Course(17.75, 390));
        Aircraft a3 = new Aircraft(789, AircraftType.L_39, odessa, new Course(160.95, 380));
        Aircraft a4 = new Aircraft(223, AircraftType.B_777, odessa, new Course(-175.6, 337));
        Aircraft a5 = new Aircraft(332, AircraftType.L_39, kiev, new Course(-28.95, 275));
        Helicopter h1 = new Helicopter(666, HelicopterType.MI_17, kiev, new Course(130.75, 240));
        Helicopter h2 = new Helicopter(777, HelicopterType.MI_171, kiev, new Course(-56.75, 220));
        Helicopter h3 = new Helicopter(888, HelicopterType.MI_2, odessa, new Course(-10.75, 195));
        Helicopter h4 = new Helicopter(911, HelicopterType.MI_2, odessa, new Course(14.75, 175));
        Helicopter h5 = new Helicopter(313, HelicopterType.MI_17, kiev, new Course(30.75, 240));

        dispatcher.addFlier(a1);
        dispatcher.addFlier(a2);
        dispatcher.addFlier(a3);
        dispatcher.addFlier(a4);
        dispatcher.addFlier(a5);
        dispatcher.addFlier(h1);
        dispatcher.addFlier(h2);
        dispatcher.addFlier(h3);
        dispatcher.addFlier(h4);
        dispatcher.addFlier(h5);

        threadPool.submit(a1);
        threadPool.submit(h1);
        threadPool.submit(a2);
        threadPool.submit(h2);
        threadPool.submit(a3);
        threadPool.submit(h3);
        threadPool.submit(a4);
        threadPool.submit(h4);
        threadPool.submit(a5);
        threadPool.submit(h5);

        try {
            Thread.sleep(60_000);
        } catch (InterruptedException ignored) {
        }

        dispatcher.stop();
        threadPool.awaitTermination(5, TimeUnit.SECONDS);
        threadPool.shutdown();
    }
}