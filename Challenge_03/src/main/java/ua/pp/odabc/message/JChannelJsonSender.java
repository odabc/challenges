package ua.pp.odabc.message;

import com.google.gson.Gson;
import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import ua.pp.odabc.exception.JChannelNotCreatingException;
import ua.pp.odabc.exception.JChannelJsonNotSendingException;

import java.util.Queue;

class JChannelJsonSender extends ReceiverAdapter implements JChannelJson {
    protected final JChannel channel;
    protected final Gson gson;

    public JChannelJsonSender(String nameCluster) throws JChannelNotCreatingException {
        try {
            channel = new JChannel(); // use the default config, udp.xml
            channel.setReceiver(this);
            channel.connect(nameCluster);
        } catch (Exception fail) {
            throw new JChannelNotCreatingException();
        }
        gson = new Gson();
    }

    @Override
    public Queue<String> getJsonQueue() {
        return null;
    }

    @Override
    public void sendJson(Object object) throws JChannelJsonNotSendingException {
        String json = gson.toJson(object);

        Message message = new Message(null, null, json);
        try {
            channel.send(message);
        } catch (Exception fail) {
            throw new JChannelJsonNotSendingException();
        }
    }

    @Override
    public void close() {
        channel.close();
    }
}
