package ua.pp.odabc.message;

import ua.pp.odabc.exception.JChannelNotCreatingException;

public class ClusterChannelFactory {
    private final static String CLUSTER_NAME = "FlySimulator";

    private ClusterChannelFactory() {
    }

    public static JChannelJson getReceiver() throws JChannelNotCreatingException {
        return new JChannelJsonReceiver(CLUSTER_NAME);
    }

    public static JChannelJson getSender() throws JChannelNotCreatingException {
        return new JChannelJsonSender(CLUSTER_NAME);
    }

    public static String getClusterName() {
        return CLUSTER_NAME;
    }
}
