package ua.pp.odabc.message;

import org.jgroups.Message;
import ua.pp.odabc.exception.JChannelNotCreatingException;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

class JChannelJsonReceiver extends JChannelJsonSender {
    private final Queue<String> messages = new ConcurrentLinkedQueue<>();

    public JChannelJsonReceiver(String nameCluster) throws JChannelNotCreatingException {
        super(nameCluster);
    }

    @Override
    @Deprecated
    public void receive(Message msg) {
        String json = (String) msg.getObject();
        messages.add(json);
    }

    @Override
    public Queue<String> getJsonQueue() {
        return messages;
    }
}

