package ua.pp.odabc.message;

import ua.pp.odabc.exception.JChannelJsonNotSendingException;

import java.util.Queue;

public interface JChannelJson {
    Queue<String> getJsonQueue();
    void sendJson(Object object) throws JChannelJsonNotSendingException;
    void close();

}
