package ua.pp.odabc.aviation;

import com.google.gson.annotations.Expose;
import ua.pp.odabc.entity.Course;
import ua.pp.odabc.entity.Location;
import ua.pp.odabc.exception.JChannelNotCreatingException;
import ua.pp.odabc.exception.JChannelJsonNotSendingException;
import ua.pp.odabc.message.JChannelJson;
import ua.pp.odabc.message.ClusterChannelFactory;

public abstract class Flier<E> implements Flyable, Runnable {
    private final int id;
    private final E type;
    private final Location location;
    private final Course course;

    @Expose(serialize = false, deserialize = false)
    private transient final JChannelJson channel;
    // Gson annotation doesn't work with inheritance
    @Expose(serialize = false, deserialize = false)
    private volatile boolean onLand = true;

    public Flier(int id, E type, Location location, Course course) throws JChannelNotCreatingException {
        this.id = id;
        this.type = type;
        this.location = location;
        this.course = course;

        channel = ClusterChannelFactory.getSender();
    }

    public int getId() {
        return id;
    }

    @Override
    public void setLocation(Location location) {
        synchronized (this.location) {
            this.location.setAltitude(location.getAltitude());
            this.location.setLatitude(location.getLatitude());
            this.location.setLongitude(location.getLongitude());
        }
    }

    @Override
    public void setCourse(Course course) {
        synchronized (this.course) {
            this.course.setRoute(course.getRoute());
            this.course.setSpeed(course.getSpeed());
        }
    }

    @Override
    public void takeoff() {
        try {
            channel.sendJson(this);
        } catch (JChannelJsonNotSendingException fail) {
            System.err.println("Channel fail");
        }
        onLand = false;
    }

    @Override
    public void flight(double time) {
        // distance in km and speed in km/h, time in sec
        double dist = course.getSpeed() * time / 3600;

        double incLatitude = dist * Math.cos(Math.toRadians(course.getRoute())) / Location.KM_IN_DEGREE;
        double incLongitude = dist * Math.sin(Math.toRadians(course.getRoute())) / Location.KM_IN_DEGREE;
        double altitude = Math.random() * 5_000 + 7_000;

        synchronized (this.location) {
            location.setLatitude(location.getLatitude() + incLatitude);
            location.setLongitude(location.getLongitude() + incLongitude);
            location.setAltitude(altitude);
        }
    }

    @Override
    public void landing() {
        onLand = true;
        try {
            channel.sendJson(this);
        } catch (JChannelJsonNotSendingException fail) {
            System.err.println("Channel fail");
        }
    }

    @Override
    public void run() {
        double timeFlyingStep = 0.1;
        double timeMessageDelay = 10.0;

        takeoff();
        int timeFlyingDelay = (int) (timeFlyingStep * 1000);
        double needSendMessage = timeMessageDelay;

        while (!onLand) {
            flight(timeFlyingStep);

            try {
                Thread.sleep(timeFlyingDelay);

                needSendMessage -= timeFlyingStep;
                if (needSendMessage <= 0) {
                    try {
                        channel.sendJson(this);
                    } catch (JChannelJsonNotSendingException fail) {
                        System.err.println("Channel fail");
                    }
                    needSendMessage = timeMessageDelay;
                }
            } catch (Exception fail) {
                landing();
            }
        }
        channel.close();
    }
}
