package ua.pp.odabc.aviation;

import ua.pp.odabc.entity.Course;
import ua.pp.odabc.entity.Location;
import ua.pp.odabc.exception.JChannelNotCreatingException;

public class Helicopter extends Flier {

    public Helicopter(int id, HelicopterType type, Location location, Course course) throws JChannelNotCreatingException {
        super(id, type, location, course);
    }
}
