package ua.pp.odabc.aviation;

public enum AircraftType {
    L_39,
    A_300,
    B_777
}
