package ua.pp.odabc.aviation;

import ua.pp.odabc.entity.Course;
import ua.pp.odabc.entity.Location;

public interface Flyable {

    /**
     * @param course - set new course values
     */
    void setCourse(Course course);

    /**
     * @param location - set new location values
     */
    void setLocation(Location location);

    void takeoff();

    /**
     * Change location depending on course
     * @param time - time of flight in sec
     */
    void flight(double time);

    void landing();
}
