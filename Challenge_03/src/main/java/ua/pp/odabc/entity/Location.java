package ua.pp.odabc.entity;

/**
 *
 * latitude, longitude in degree
 * altitude in meters
 * from GeoDataSource
 * South latitudes are negative, east longitudes are positive
 * Location odessa = new Location(46.4775, 30.7326, 0);
 * Location kiev = new Location(50.4547, 30.5238, 0);
 */
public class Location {
    public static final double KM_IN_DEGREE = 60 * 1.853159616;

    private double latitude;
    private double longitude;
    private double altitude;

    public Location(double latitude, double longitude, double altitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }
}
