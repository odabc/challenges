package ua.pp.odabc.entity;

/**
 *
 * route in degree
 * speed in km/h
 */
public class Course {
    private double route;
    private double speed;

    public Course(double route, double speed) {
        this.route = route;
        this.speed = speed;
    }

    public double getRoute() {
        return route;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void setRoute(double route) {
        this.route = route;
    }
}
